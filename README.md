# demo-makefile

入门Makefile

## 编译项目

```shell
# 把源代码编译成可重定位目标文件
g++ -c main.cpp hello.cpp factorial.cpp 
# 链接目标文件成可执行文件
g++ main.o  hello.o  factorial.o 
./a.out 
```
也可以省略上面的步骤，一次性完成预处理、编译、汇编、链接步骤，直接生成可执行文件
```shell
g++ main.cpp  hello.cpp  factorial.cpp
```
