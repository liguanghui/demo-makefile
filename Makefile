# This is a comment line
CC = g++
# CFLAGS will be the options passed to the compiler.
CFLAGS = -c -Wall
OBJECTS = main.o hello.o factorial.o

all: prog

prog: $(OBJECTS)
	$(CC) $(OBJECTS) -o $@
%.o: %.cpp
	$(CC) $(CFLAGS) $<
clean:
	rm -f *.o prog

